//
// PlasmaRifle.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:06:19 2014 Jean Gravier
// Last update Fri Jan 17 15:27:29 2014 Jean Gravier
//

#ifndef _PLASMARIFLE_H_
# define _PLASMARIFLE_H_

#include <string>
#include "AWeapon.hh"

class			PlasmaRifle: public AWeapon
{
public:
  PlasmaRifle(std::string const& = "Plasma Rifle", int = 5, int = 21);
  ~PlasmaRifle();

public:
  void			attack() const;
};

#endif /* _PLASMARIFLE_H_ */
