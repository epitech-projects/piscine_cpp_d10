//
// Character.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:53:34 2014 Jean Gravier
// Last update Fri Jan 17 16:32:44 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "Character.hh"
#include "AEnemy.hh"
#include "AWeapon.hh"

Character::Character(std::string const& name): _name(name)
{
  this->_ap = 40;
  this->_weapon = NULL;
}

Character::~Character()
{

}

std::string const&	Character::getName() const
{
  return (this->_name);
}

int			Character::getAP() const
{
  return (this->_ap);
}

AWeapon			*Character::getWeapon() const
{
  return (this->_weapon);
}

void		Character::recoverAP()
{
  if (this->_ap <= 30)
    this->_ap += 10;
}

void		Character::attack(AEnemy* enemy)
{
  if ((this->_ap - this->_weapon->getAPCost()) >= 0)
    {
      this->_ap -= this->_weapon->getAPCost();
      std::cout << this->_name << " attacks " << enemy->getType() << " with a "
		<< this->_weapon->getName() << std::endl;
      this->_weapon->attack();
      enemy->takeDamage(this->_weapon->getDamage());
      if (enemy->getHP() <= 0)
	delete enemy;
    }
}

void		Character::equip(AWeapon* weapon)
{
  this->_weapon = weapon;
}

std::ostream	&operator<<(std::ostream &stream, Character const& character)
{
  AWeapon *weapon;

  weapon = character.getWeapon();
  if (weapon == NULL)
    stream << character.getName() << " has " << character.getAP() << " AP and is unarmed" << std::endl;
  else
    stream << character.getName() << " has " << character.getAP() << " AP and wields a "
	   << weapon->getName() << std::endl;
  return (stream);
}
