//
// AWeapon.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 14:58:00 2014 Jean Gravier
// Last update Fri Jan 17 16:27:46 2014 Jean Gravier
//

#include <string>
#include "AWeapon.hh"

AWeapon::AWeapon(std::string const& name, int apcost, int damage): _name(name), _apcost(apcost), _damage(damage)
{

}

AWeapon::~AWeapon()
{

}

std::string const&	AWeapon::getName() const
{
  return (this->_name);
}
int			AWeapon::getAPCost() const
{
  return (this->_apcost);
}
int			AWeapon::getDamage() const
{
  return (this->_damage);
}
