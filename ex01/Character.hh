//
// Character.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:53:37 2014 Jean Gravier
// Last update Fri Jan 17 16:18:30 2014 Jean Gravier
//

#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#include <string>
#include <iostream>
#include <fstream>
#include "AWeapon.hh"
#include "AEnemy.hh"

class			Character
{
public:
  Character(std::string const&);
  ~Character();

public:
  std::string const&	getName() const;
  int			getAP() const;
  AWeapon*		getWeapon() const;

public:
  void			recoverAP();
  void			equip(AWeapon *);
  void			attack(AEnemy *);

private:
  std::string		_name;
  int			_ap;
  AWeapon*		_weapon;
};

std::ostream		&operator<<(std::ostream&, Character const&);

#endif /* _CHARACTER_H_ */
