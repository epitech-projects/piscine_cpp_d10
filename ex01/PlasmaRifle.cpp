//
// PlasmaRifle.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:06:10 2014 Jean Gravier
// Last update Fri Jan 17 15:42:10 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "PlasmaRifle.hh"

PlasmaRifle::PlasmaRifle(std::string const& name, int apcost, int damage): AWeapon(name, apcost, damage)
{

}

PlasmaRifle::~PlasmaRifle()
{

}

void		PlasmaRifle::attack() const
{
  std::cout << "* piouuu piouuu piouuu *" << std::endl;
}
