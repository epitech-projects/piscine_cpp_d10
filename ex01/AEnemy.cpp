//
// AEnemy.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:31:09 2014 Jean Gravier
// Last update Fri Jan 17 16:24:30 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "AEnemy.hh"

AEnemy::AEnemy(int hp, std::string const& type): _hp(hp), _type(type)
{

}

AEnemy::~AEnemy()
{

}

std::string	AEnemy::getType() const
{
  return (this->_type);
}

int		AEnemy::getHP() const
{
  return (this->_hp);
}

void		AEnemy::takeDamage(int damage)
{
  if (damage >= 0)
    {
      this->_hp -= damage;
    }
}
