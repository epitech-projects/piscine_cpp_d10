//
// AEnemy.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:31:23 2014 Jean Gravier
// Last update Fri Jan 17 16:24:18 2014 Jean Gravier
//

#ifndef _AENEMY_H_
#define _AENEMY_H_

#include <string>

class			AEnemy
{
public:
  AEnemy(int, std::string const&);
  virtual ~AEnemy();

public:
  std::string		getType() const;
  int			getHP() const;
  virtual void		takeDamage(int);

protected:
  int			_hp;
  std::string		_type;
};

#endif /* _AENEMY_H_ */
