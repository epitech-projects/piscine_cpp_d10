//
// SuperMutant.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:41:44 2014 Jean Gravier
// Last update Fri Jan 17 16:24:46 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "AEnemy.hh"
#include "SuperMutant.hh"

SuperMutant::SuperMutant(int hp, std::string const& type): AEnemy(hp, type)
{
  std::cout << "Gaaah. Me want smash heads !" << std::endl;
}

SuperMutant::~SuperMutant()
{
  std::cout << "Aaargh ..." << std::endl;
}

void		SuperMutant::takeDamage(int damage)
{
  if (damage >= 0)
    this->_hp -= (damage - 3);
}
