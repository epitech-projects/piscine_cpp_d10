//
// PowerFist.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:06:19 2014 Jean Gravier
// Last update Fri Jan 17 15:29:26 2014 Jean Gravier
//

#ifndef _POWERFIST_H_
# define _POWERFIST_H_

#include <string>
#include "AWeapon.hh"

class			PowerFist: public AWeapon
{
public:
  PowerFist(std::string const& = "Power Fist", int = 8, int = 50);
  ~PowerFist();

public:
  void			attack() const;
};

#endif /* _POWERFIST_H_ */
