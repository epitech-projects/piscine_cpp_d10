//
// AWeapon.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 14:58:03 2014 Jean Gravier
// Last update Fri Jan 17 16:26:54 2014 Jean Gravier
//

#ifndef _AWEAPON_H_
# define _AWEAPON_H_

#include <string>

class			AWeapon
{
public:
  AWeapon(std::string const&, int, int);
  ~AWeapon();

public:
  std::string const&	getName() const;
  int			getAPCost() const;
  int			getDamage() const;

public:
  virtual void		attack() const = 0;

protected:
  std::string		_name;
  int			_apcost;
  int			_damage;
};

#endif /* _AWEAPON_H_ */
