//
// SuperMutant.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:42:21 2014 Jean Gravier
// Last update Fri Jan 17 15:51:10 2014 Jean Gravier
//

#ifndef _SUPERMUTANT_H_
#define _SUPERMUTANT_H_

#include "AEnemy.hh"
#include <string>

class			SuperMutant: public AEnemy
{
public:
  SuperMutant(int = 170, std::string const& = "Super Mutant");
  virtual ~SuperMutant();

public:
  void			takeDamage(int);
};

#endif /* _SUPERMUTANT_H_ */
