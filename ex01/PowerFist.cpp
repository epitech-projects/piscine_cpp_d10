//
// PowerFist.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:06:10 2014 Jean Gravier
// Last update Fri Jan 17 15:29:32 2014 Jean Gravier
//

#include <string>
#include <iostream>
#include "PowerFist.hh"

PowerFist::PowerFist(std::string const& name, int apcost, int damage): AWeapon(name, apcost, damage)
{

}

PowerFist::~PowerFist()
{

}

void		PowerFist::attack() const
{
  std::cout << "* pschhh... SBAM! *" << std::endl;
}
