//
// RadScorpion.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 15:42:21 2014 Jean Gravier
// Last update Fri Jan 17 15:50:37 2014 Jean Gravier
//

#ifndef _RADSCORPION_H_
#define _RADSCORPION_H_

#include <string>
#include <iostream>
#include "AEnemy.hh"

class			RadScorpion: public AEnemy
{
public:
  RadScorpion(int = 80, std::string const& = "RadScorpion");
  virtual ~RadScorpion();

public:
  void			takeDamage(int);
};

#endif /* _RADSCORPION_H_ */
