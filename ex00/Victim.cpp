//
// Victim.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 12:21:02 2014 Jean Gravier
// Last update Fri Jan 17 14:54:46 2014 Jean Gravier
//

#include <string>
#include <fstream>
#include <iostream>
#include "Victim.hh"

Victim::Victim(std::string const& name): _name(name)
{
  std::cout << "Some random victim called " << this->_name << " just popped !" << std::endl;
}

Victim::~Victim()
{
    std::cout << "Victim " << this->_name << " just died for no apparent reason !" << std::endl;
}

std::string const&	Victim::getName() const
{
  return (this->_name);
}

void			Victim::getPolymorphed() const
{
  std::cout << this->_name << " has been turned into a cute little sheep !" << std::endl;
}

std::ostream	&operator<<(std::ostream &stream, Victim const& victim)
{
  stream << "I'm " << victim.getName() << " and i like otters !" << std::endl;
  return (stream);
}
