//
// Peon.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 12:55:26 2014 Jean Gravier
// Last update Fri Jan 17 14:43:08 2014 Jean Gravier
//

#include "Peon.hh"
#include <string>
#include <iostream>

Peon::Peon(std::string const& name): Victim(name)
{
  std::cout << "Zog zog." << std::endl;
}

Peon::~Peon()
{
  std::cout << "Bleuark..." << std::endl;
}

void		Peon::getPolymorphed() const
{
  std::cout << this->_name << " has been turned into a pink pony !" << std::endl;
}
