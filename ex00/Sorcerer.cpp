//
// Sorcerer.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 12:07:45 2014 Jean Gravier
// Last update Fri Jan 17 14:54:29 2014 Jean Gravier
//

#include "Sorcerer.hh"
#include <string>
#include <iostream>
#include <fstream>
#include "Victim.hh"

Sorcerer::Sorcerer(std::string const& name, std::string const& title): _name(name), _title(title)
{
  std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;
}

Sorcerer::~Sorcerer()
{
  std::cout << this->_name << ", " << this->_title
	    << ", is dead. Consequences will never be the same !" << std::endl;
}

std::string const&	Sorcerer::getName() const
{
  return (this->_name);
}

std::string const&	Sorcerer::getTitle() const
{
  return (this->_title);
}

std::ostream	&operator<<(std::ostream &stream, Sorcerer const& sorcerer)
{
  stream << "I am " << sorcerer.getName() << ", " << sorcerer.getTitle() << ", and I like ponies !" << std::endl;
  return (stream);
}

void			Sorcerer::polymorph(Victim const& victim) const
{
  victim.getPolymorphed();
}
