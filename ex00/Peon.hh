//
// Peon.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 12:55:45 2014 Jean Gravier
// Last update Fri Jan 17 14:43:33 2014 Jean Gravier
//

#ifndef _PEON_H_
#define _PEON_H_

#include <string>
#include "Victim.hh"

class			Peon: public Victim
{
public:
  Peon(std::string const&);
  ~Peon();

public:
  void			getPolymorphed() const;
};

#endif /* _PEON_H_ */
