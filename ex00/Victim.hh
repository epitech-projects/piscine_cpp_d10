//
// Victim.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 12:20:50 2014 Jean Gravier
// Last update Fri Jan 17 16:57:55 2014 Jean Gravier
//
#ifndef _VICTIM_H_
#define _VICTIM_H_

#include <string>
#include <fstream>

class			Victim
{
public:
  Victim(std::string const&);
  virtual ~Victim();

public:
  std::string const&	getName() const;
  virtual void			getPolymorphed() const;

protected:
  std::string		_name;
};

std::ostream	&operator<<(std::ostream &, Victim const&);

#endif /* _VICTIM_H_ */
