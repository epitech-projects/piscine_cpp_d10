//
// Sorcerer.hh for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d10/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Fri Jan 17 12:05:46 2014 Jean Gravier
// Last update Fri Jan 17 14:47:19 2014 Jean Gravier
//

#ifndef _SORCERER_H_
#define _SORCERER_H_

#include <string>
#include <fstream>
#include "Victim.hh"

class			Sorcerer
{
public:
  Sorcerer(std::string const&, std::string const&);
  ~Sorcerer();

public:
  std::string const&	getName() const;
  std::string const&	getTitle() const;
  void			polymorph(Victim const&) const;
protected:
  std::string		_name;
  std::string		_title;
};

std::ostream		&operator<<(std::ostream &, Sorcerer const&);

#endif /* _SORCERER_H_ */
